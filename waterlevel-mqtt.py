#!/usr/bin/env python3
import json
import sys
from periphery import GPIO
from apscheduler.schedulers.blocking import BlockingScheduler
import paho.mqtt.client as mqtt
import os


HOSTNAME = os.uname()[1].split('.')[0]
MQTT_STATE_TOPIC="homeassistant/binary_sensor/" + HOSTNAME + "_waterlevel_low/state"

if os.uname()[1] == "rpi02":
    GPIO_PIN=6 # rpi02 test setup
else:
    GPIO_PIN=20 # rpi01 waterlevel


#def init():
#    print("init waterlevel_low " + str(bool(gpio_in.read())))

def update_waterlevel():
    waterlevel_state = str(bool(gpio_in.read()))
    client.publish(MQTT_STATE_TOPIC, waterlevel_state)
    print("waterlevel_low " + waterlevel_state)

if __name__ == '__main__':
    try:
#        init()
        gpio_in = GPIO("/dev/gpiochip0", GPIO_PIN, "in")
        scheduler = BlockingScheduler()
        client = mqtt.Client(client_id=HOSTNAME + "-python-waterlevel", clean_session=False)
        client.will_set(MQTT_STATE_TOPIC, "False")
        client.username_pw_set(os.environ['MQTT_USER'],os.environ['MQTT_PASSWORD'])
        client.connect("mosquitto.pamperspoil.com")
        client.loop_start()
        config_payload = {
            "name": HOSTNAME + "-waterlevel-monitor",
            "unique_id": HOSTNAME + "-sensor",
            "device_class": "problem",
            "payload_on": "False",
            "payload_off": "True",
            "state_topic": MQTT_STATE_TOPIC
            }
        client.publish("homeassistant/binary_sensor/" + HOSTNAME + "_waterlevel_low/config", json.dumps(config_payload), retain=True)
        print(" published config to homeassistant/binary_sensor/" + HOSTNAME + "_waterlevel_low/config" + json.dumps(config_payload))
        # scheduler.add_job(update_waterlevel, 'cron', minute='*/1')
        scheduler.add_job(update_waterlevel, 'cron', second='*/1')
        update_waterlevel()
        scheduler.start()

    except KeyboardInterrupt:
        gpio_in.close()
