# Scope
Fedora IoT on Raspberry Pi 3 Python sensor for water irrigation system. GPIO state (edge detection) is pushed to MQTT.

# Fedora IoT to Rpi3 installation procedure
Get latest raw image from https://getfedora.org/en/iot/download/
```
$ wget https://download.fedoraproject.org/pub/alt/iot/40/IoT/aarch64/images/Fedora-IoT-raw-40-20240422.3.aarch64.raw.xz
$ sudo rpm-ostree install arm-image-installer --apply-live
# important: logout from any automounting gui sessions to avoid installation issues
# or: $ sudo umount /dev/sda1
$ sudo arm-image-installer --image=Fedora-IoT-raw-40-20240422.3.aarch64.raw.xz --target=rpi3 --media=/dev/sda --resizefs --addkey ~/.ssh/id_ecdsa.pub

# configure wlan
$ sudo mount /dev/sda3 /mnt
$ cd /mnt/ostree/deploy/fedora-iot/deploy/fd771dcf351bfc9067eb64de2e2b3870f8a9cdcad9a74886066aeab055e41e9e.0/etc/NetworkManager/system-connections/
# render file from templates/ssid.connection.j2
$ sudo chmod 600 ssid.connection

# set hostname
$ sudo vi /mnt/ostree/deploy/fedora-iot/deploy/fd771dcf351bfc9067eb64de2e2b3870f8a9cdcad9a74886066aeab055e41e9e.0/etc/hostname

$ sudo umount /mnt
# eject sd card

# insert sd card into rpi and boot
$ ssh root@device-609
```

# ssh login
`ssh root@<rpi-hostname>` using ssh key

# create user
```
useradd -p "*" -U -m hp -G wheel
mkdir /var/home/hp/.ssh
cp ~/.ssh/id_ecdsa.pub /var/home/hp/.ssh/authorized_keys
chown hp:hp /var/home/hp/.ssh -R
vi /etc/sudoers.d/00-ansible # hp ALL=(ALL) NOPASSWD:ALL
```


# obsolete: wlan bring up
```
$ nmcli device wifi list
$ nmcli device wifi connect <ssid> password <pw redacted>
$ nmcli general status
$ nmcli connection show
$ nmcli connection show --active
```


# WA: Blocked/Workaround: gpio - RPi.GPIO2 - Edge Detection NOK
As there is no working edge detecion on fedora iot known to the author, we poll the state every second as a workaround. Polling has further advantage of debounce and may fit as a permanent solution.


# Legacy Notes

```
sudo rpm-ostree install python3-libgpiod --apply-live # will fail because 1Gram+1G/dev/zram0 will lead to memory exhaustion on 36.20221102.0 ... increase /dev/zram0 ?

swapoff /dev/zram0
zramctl --reset /dev/zram0
zramctl --find --size 4096M
mkswap /dev/zram0
swapon /dev/zram0

sudo rpm-ostree install gcc python-devel python-pip --apply-live
pip install wheel
pip install -r requirements.txt --user

$ sudo ./non_root_permission.sh

$ sudo su -
# usermod -aG gpio hp
# grep -E '^gpio:' /usr/lib/group >> /etc/group

```


# Market Analysis RPI GPIO Libraries
  URL                                                                     Stars       Last Update
- https://github.com/vsergeev/python-periphery                            489         11.07.2023
- https://github.com/underground-software/RPi.GPIO2                       17          10.2022 https://github.com/underground-software/RPi.GPIO2/issues/61
- https://pypi.org/project/mqtt-io/ https://mqtt-io.app/2.2.7/#/          349         https://github.com/flyte/mqtt-io/issues/277
- https://github.com/brgl/libgpiod (used by RPi.GPIO2)                    182         10.2020
- https://sourceforge.net/projects/raspberry-gpio-python/                 Sourceforge 02.2022 RPi.GPIO (deprecating)
- https://github.com/hhk7734/python3-gpiod                                28          02.2022 ... no callback: https://github.com/hhk7734/python3-gpiod/issues/8


## NOK: RPi.GPIO2
https://github.com/underground-software/RPi.GPIO2/issues/61

## NOK: Failed to add edge detection using mqtt.io
https://stackoverflow.com/questions/41679547/failed-to-add-edge-detection-raspberry-pi-3-gpio

```
GPIO.add_event_detect(GPIO_PIN, GPIO.BOTH, callback=update_waterlevel, bouncetime=200)
RuntimeError: Failed to add edge detection
```

## gpiod.py
have not been able to debounce [gpiomon.py](https://git.kernel.org/pub/scm/libs/libgpiod/libgpiod.git/tree/bindings/python/examples/gpiomon.py)

## Fedora on RPi.GPIO
https://fedoraproject.org/wiki/Architectures/ARM/Raspberry_Pi#Is_GPIO_supported?
rpi.gpio is not currently supported. use libgpiod


# WIP: Rpi3 Wiring
- 5V (Red)
- GND (Black)
- Temperture OneWire (Yellow + Orange?)
- Water Low Sensor (Yellow?)    GPIO20  (Header Pin 38)
